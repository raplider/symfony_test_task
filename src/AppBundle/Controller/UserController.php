<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    public function viewAction(Request $request)
    {
        return $this->render('user/view.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    public function editAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(UserEditType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_view');
        }

        return $this->render('user/edit.html.twig', [
            //'user' => $this->getUser(),
            'form' => $form->createView(),
        ]);
    }

}
