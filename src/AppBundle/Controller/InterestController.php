<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Interest;
use AppBundle\Form\InterestEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class InterestController extends Controller
{
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Interest::class);
        //$interests = $repository->findAll();
        $interests = $repository->findBy([], ['id' => 'ASC']);
        return $this->render('interest/index.html.twig', [
            'interests' => $interests,
        ]);
    }

    public function editAction(Request $request, Interest $interest)
    {
        $form = $this->createForm(InterestEditType::class, $interest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($interest);
            $em->flush();

            return $this->redirectToRoute('interest_index');
        }
        return $this->render('interest/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function createAction(Request $request)
    {
        $interest = new Interest();
        $form = $this->createForm(InterestEditType::class, $interest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($interest);
            $em->flush();

            return $this->redirectToRoute('interest_index');
        }
        return $this->render('interest/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
